# MQTT-backed thermometer

## Hardware
- NodeMCU v2
- AM2120 thermometer

## Images
![](https://hackster.imgix.net/uploads/attachments/267054/dht22-pinout_2P1AgF3wPs.png?auto=compress%2Cformat&w=680&h=510&fit=max)

## References
- [Guide for AM2120 (CZ)](https://navody.arduino-shop.cz/navody-k-produktum/teplomer-a-vlhkomer-am2120.html)