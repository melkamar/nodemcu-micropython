import dht
from machine import Pin
from utime import sleep_ms, ticks_ms, ticks_diff

from lib import NodemcuPin


class AM2120:
    """
    Adapter for the AM2120 temperature and humidity sensor.

    The sensor uses the same protocol as the DHT22-class of sensors. During testing
    I found out that in most cases every second read fails. I don't know why and the
    easiest fix is to just retry reading from the sensor again. That's what this class
    does.
    """

    def __init__(
            self,
            pin: Pin,
            verbose: bool = False,
            cache_ttl_ms: int = 2500):
        """
        :param pin: the data pin where to read the values
        :param cache_ttl_ms: as per the documentation, the sensor should be called at most once every
                             two seconds. In-memory "cache" is be used to return the previously
                             saved results if there has not been enough time since the last sensor
                             read. This parameter configures the length of time between two
                             consecutive real readings.
        """
        self._dht_reader = dht.DHT22(pin)
        self._verbose = verbose
        self._cache_ttl_ms = cache_ttl_ms

        self._last_read_ticks = None
        self._cached_temperature = None
        self._cached_humidity = None

    def read_values(
            self,
            read_attempts: int = 5,
            backoff_ms: int = 150,
            allow_cache: bool = True
    ):
        """
        :param read_attempts: number of read attempts to make before giving up and returning None
        :param backoff_ms: milliseconds to wait before retrying to read the sensor if the previous
                           read failed
        :param allow_cache: use the cache. See cache_ttl_ms parameter of this class for more details.
        :return: tuple (temp: float, humidity: float).
                 temp is in degrees Celsius, humidity a percentage.
                 Both values may also be None if the reading from the sensor fails multiple times
                 in a row.
        """
        read_ok = False

        if allow_cache and self._should_read_from_cache():
            self._debug('AM2120 cache hit, returning stored values')
            return self._cached_temperature, self._cached_humidity

        self._debug('AM2120 cache miss')

        for i in range(read_attempts):
            self._debug('AM2120 read attempt {}/{}'.format(i, read_attempts-1))

            try:
                self._dht_reader.measure()
                read_ok = True
                self._debug('AM2120: Read ok: {} / {}'.format(
                    self._dht_reader.temperature(),
                    self._dht_reader.humidity())
                )
                break
            except Exception as e:
                self._debug('AM2120 attempt to read failed: {}'.format(e))
                sleep_ms(backoff_ms)

        if read_ok:
            self._cached_temperature = self._dht_reader.temperature()
            self._cached_humidity = self._dht_reader.humidity()
            self._last_read_ticks = ticks_ms()
            return self._dht_reader.temperature(), self._dht_reader.humidity()

        self._debug('AM2120: Read failed. Giving up.')

        return None, None

    def _debug(self, message: str):
        if self._verbose:
            print(message)

    def _should_read_from_cache(self):
        if self._last_read_ticks is None or \
                self._cached_temperature is None or \
                self._cached_humidity is None:
            return False

        ticks_now = ticks_ms()
        self._debug('AM2120 cache: now: {} vs last: {} (ttl: {})'.format(
            ticks_now, self._last_read_ticks, self._cache_ttl_ms)
        )

        return ticks_diff(ticks_now, self._last_read_ticks) < self._cache_ttl_ms


def main():
    print('Works.')
    pin = NodemcuPin.get_pin_read(4)
    am = AM2120(pin, True)
    while True:
        temp, humid = am.read_values()
        print('-'*80)
        print('temp:    {} °C'.format(temp))
        print('humid:   {} %'.format(humid))
        print('-'*80)
        sleep_ms(500)


if __name__ == '__main__':
    main()
