# AM2120 example

This is an example script using the [AM2120 adapter class](../../lib/lib.py).

## Hardware
- NodeMCU v2
- AM2120 thermometer

This was developed on MacOS. The Micropython code will run on any other platform,
but you may need to customize the [Makefile](Makefile) to work for your environment.

## Wiring up
| Sensor pin | NodeMCU pin (as printed on board) |
| ---------- | --------------------------------- |
| 1          | `VN`                              |
| 2          | `D4`                              |
| 3          | `GND`                             |
| 4          | `GND`                             |

![](https://hackster.imgix.net/uploads/attachments/267054/dht22-pinout_2P1AgF3wPs.png?auto=compress%2Cformat&w=680&h=510&fit=max)

## Running the example
```bash
$ make
>>> import run
```

## Output
The example runs in an infinite loop and prints out values of the sensor:

```
--------------------------------------------------------------------------------
(5886650)
temp:    24.3 °C
humid:   42.7 %
--------------------------------------------------------------------------------
```


## References
- [Guide for AM2120 (CZ)](https://navody.arduino-shop.cz/navody-k-produktum/teplomer-a-vlhkomer-am2120.html)