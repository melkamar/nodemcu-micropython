from utime import sleep_ms, ticks_ms

from lib import NodemcuPin, AM2120


def main():
    pin = NodemcuPin.get_pin_read(4)
    am = AM2120(pin)
    while True:
        temp, humid = am.read_values()
        print('-' * 80)
        print('({})'.format(ticks_ms()))
        print('temp:    {} °C'.format(temp))
        print('humid:   {} %'.format(humid))
        print('-' * 80)
        sleep_ms(3000)


if __name__ == '__main__':
    main()
