import json
import dht
from machine import Pin, ADC
from network import WLAN, STA_IF
from umqtt.simple import MQTTClient
from urandom import getrandbits
from utime import sleep_ms, ticks_diff, ticks_ms


def file_exists(filename):
    import os
    try:
        os.stat(filename)
        return True
    except OSError:
        return False


def randint(start, stop=None):
    """
    Get a random integer from the interval [start, stop) (start-inclusive, stop-exclusive).
    :param start:
    :param stop:
    :return: A single random integer.
    """
    if stop is None:
        stop = start
        start = 0
    upper = stop - start
    bits = 0
    pwr2 = 1
    while upper > pwr2:
        pwr2 <<= 1
        bits += 1
    while True:
        r = getrandbits(bits)
        if r < upper:
            break
    return r + start


#########

class BaseNodeMcuClass:
    def __init__(self, verbose: bool = False):
        self._verbose = verbose

    def _debug(self, message: str):
        if self._verbose:
            print(message)


class Wifi:
    def __init__(self, ssid: str, password: str):
        """
        tl;dr:

        :param ssid: the SSID of the wifi network to connect to
        :param password: the password of the wifi network
        """
        self.ssid = ssid
        self.password = password
        self._wlan = WLAN(STA_IF)

    def connect(self):
        """
        Connect the device to a wifi network. This method blocks until the connection works.
        """
        self._wlan.active(True)
        if not self._wlan.isconnected():
            self._wlan.connect(self.ssid, self.password)
            while not self._wlan.isconnected():
                pass
            print('Network "{}" connected. Config: '.format(self.ssid), self._wlan.ifconfig())

    def disconnect(self):
        """
        Disconnect the device from the current wifi network.
        :return:
        """
        if not self._wlan.isconnected():
            return

        self._wlan.disconnect()
        while self.is_connected():
            pass
        print('Network "{}" disconnected.'.format(self.ssid))

    def is_connected(self):
        return self._wlan.isconnected()


class NodemcuPin:
    # Mapping of "DX" pin to GPIO number
    PINS_MAPPING = {
        0: 16,
        1: 5,
        2: 4,
        3: 0,
        4: 2,
        5: 14,
        6: 12,
        7: 13,
        8: 15,
    }

    ANALOG_PIN_NUMBER = 0

    BUTTON_PRESSED = 0
    BUTTON_NOT_PRESSED = 1

    @staticmethod
    def get_pin_read(board_pin_number: int) -> Pin:
        """
        Get a Pin object for reading values.

        :param board_pin_number: the number of the GPIO pin as printed on the NodeMCU board. E.g. for D2,
                            use 2 as the parameter.
        :return: A Pin object.
        """
        return Pin(NodemcuPin.PINS_MAPPING[board_pin_number], Pin.IN)

    @staticmethod
    def get_pin_write(board_pin_number):
        """
        Get a Pin object for writing values.

        :param board_pin_number: the number of the GPIO pin as printed on the NodeMCU board. E.g. for D2,
                            use 2 as the parameter.
        :return: A Pin object.
        """
        return Pin(NodemcuPin.PINS_MAPPING[board_pin_number], Pin.OUT)

    @staticmethod
    def get_analog_pin_read() -> ADC:
        return ADC(NodemcuPin.ANALOG_PIN_NUMBER)


class Mqtt:
    def __init__(self, server_ip: str, user: str, password: str, wifi: Wifi = None,
                 try_wifi_reconnect: bool = True):
        """
        tl;dr:

        :param server_ip:
        :param user:
        :param password:
        :param wifi: A Wifi object to use for network related assertions and checks. If None, then
                     the user of this class is responsible for making sure that the device is
                     connected to the network.
        :param try_wifi_reconnect: If true and 'wifi' is also provided, it will be checked when
                                   sending a message. If the wifi is disconnected, it will attempt
                                   to reconnect first, before sending the MQTT message.
        """
        self._wifi = wifi
        self._try_wifi_reconnect = try_wifi_reconnect
        self._mqtt_client = MQTTClient("umqtt_client", server_ip, user=user, password=password)

        self._consecutive_send_skips = 0
        """
        The number of send failures in a row that were skipped. This is used for the logic of:
        Send todo:
        """

        try:
            self._mqtt_client.connect()
            self.ok = True
        except OSError as e:
            print('Cannot connect to the MQTT server')
            self.ok = False

    def subscribe(self, topic: str, callback):
        """Subscribe to a topic and set up a callback function."""
        if not self.ok:
            return

        # TODO: this is not tested - check if we can call subscribe on multiple topics
        self._mqtt_client.set_callback(callback)
        self._mqtt_client.subscribe(topic.encode())

    def check_msg(self):
        if not self.ok:
            return

        self._mqtt_client.check_msg()

    def send_message(self, topic: str, message: str):
        """
        Send a message to a MQTT queue.

        :param topic: the name of the topic to send the message to
        :param message: the content of the message
        :raise Exception: when sending the message fails, usually due to no network. Note that if
                          the Mqtt client has the Wifi object passed and try_wifi_reconnect is True,
                          the client will first try to reconnect to the wifi. If sending the message
                          still fails, then an Exception is raised.
        """
        if not self.ok:
            print('Mqtt: Not ok, not sending the message {}:{}'.format(topic, message))
            return

        try:
            self._handle_wifi()
            self._mqtt_client.publish(topic, message)
        except OSError:
            print('Mqtt: send failed, trying to reconnect the client')
            self._mqtt_client.connect()
            self._mqtt_client.publish(topic, message)

        print('Mqtt: sent message for "{}": "{}"'.format(topic, message))

    def _handle_wifi(self):
        """If wifi provided, check its state and try to fix, if possible."""
        if not self._wifi:
            return

        if self._wifi.is_connected():
            return

        if self._try_wifi_reconnect:
            self._wifi.connect()


class Led:
    ONBOARD_LED_PIN_NUMBER = 2

    def __init__(self, pin: Pin):
        self._pin = pin

    @staticmethod
    def get_onboard_led():
        """Get a Led object for the onboard LED."""
        led = Led(Pin(Led.ONBOARD_LED_PIN_NUMBER, Pin.OUT))
        led.off()
        return led

    def on(self):
        self._pin.value(0)

    def off(self):
        self._pin.value(1)

    def flash(self, on_duration=150, after_duration=150):
        """
        Flash a LED on and off.
        """
        self.on()
        sleep_ms(on_duration)
        self.off()
        sleep_ms(after_duration)


class MoistureSensor(BaseNodeMcuClass):
    CALIBRATION_FILE = 'calibration.json'
    CALIBRATION_KEY_MIN = 'value_min'
    CALIBRATION_KEY_MAX = 'value_max'
    CALIBRATION_VALUE_MIN_DEFAULT = 99999
    CALIBRATION_VALUE_MAX_DEFAULT = -1

    def __init__(
            self,
            power_pin: Pin,
            analog_pin: ADC,
            digital_pin: Pin,
            analog_value_min=None,
            analog_value_max=None,
            verbose: bool = False
    ):
        """
        :param power_pin:
        :param analog_pin:
        :param digital_pin:
        :param analog_value_min:
        :param analog_value_max:
        """
        super(MoistureSensor, self).__init__(verbose)

        self.power_pin = power_pin
        self.analog_pin = analog_pin
        self.digital_pin = digital_pin

        self.analog_value_min = analog_value_min if analog_value_min \
            else self.CALIBRATION_VALUE_MIN_DEFAULT

        self.analog_value_max = analog_value_max if analog_value_max \
            else self.CALIBRATION_VALUE_MAX_DEFAULT

        self._load_calibration_file()

    def read_moisture_percent(self) -> float:
        """
        Obtain the percent moisture value from the sensor. The value is normalized based on the past
        encountered minimum and maximum values.

        :return: The moisture percent in range [0, 100].
        """
        analog_val, _ = self._read_moisture_raw()
        analog_normalized = self._normalize_analog_moisture(analog_val)

        return analog_normalized

    def reset_calibration(self) -> None:
        """
        Reset all of the encountered data for calibration. Any high data read by the sensor after
        the reset will be considered 100%. Any low data will be considered 0%. After doing the
        reset, you should manipulate the physical device so that it gets the biggest low and high
        values possible.
        """
        print('Moisture: resetting calibration')

        self._update_calibrated_values(
            self.CALIBRATION_VALUE_MIN_DEFAULT,
            self.CALIBRATION_VALUE_MAX_DEFAULT
        )

    def _load_calibration_file(self):
        """
        Load the calibration data into the class fields from a file.
        """
        if not file_exists(self.CALIBRATION_FILE):
            print('Moisture: no prior calibration data available')
            return

        with open(self.CALIBRATION_FILE) as f:
            calibration_data = json.load(f)

        try:
            self.analog_value_min = calibration_data[self.CALIBRATION_KEY_MIN]
            self.analog_value_max = calibration_data[self.CALIBRATION_KEY_MAX]
            print('Moisture: reading the calibration data from a file: {}/{}'.format(
                self.analog_value_min, self.analog_value_max))
        except KeyError:
            print('Moisture: could not load calibration data from a file')
            return

    def _update_calibrated_values(self, calibration_min: int, calibration_max: int):
        """
        Update the minimum and maximum values read by the moisture sensor. The method stores the new
        values internally and also writes them to the persistent storage.

        :param calibration_min: The new minimum moisture value
        :param calibration_max: The new maximum moisture value
        :return:
        """
        print('Moisture: updating calibration data to min/max: {} {}'.format(
            calibration_min,
            calibration_max)
        )
        with open('calibration.json', 'w') as f:
            json.dump({
                self.CALIBRATION_KEY_MIN: calibration_min,
                self.CALIBRATION_KEY_MAX: calibration_max
            }, f)

        self.analog_value_min = calibration_min
        self.analog_value_max = calibration_max

    def _read_moisture_raw(self):
        """
        Read the values of both the digital and analog pins. The output values are not normalized in
        any way.

        :return: Tuple of (analog_value, digital_value)
        """
        self.power_pin.on()
        sleep_ms(500)

        analog_sample_count = 20
        analog_val_sum = 0
        for i in range(analog_sample_count):
            analog_val_sum += self.analog_pin.read()
            sleep_ms(50)

        analog_val_averaged = analog_val_sum // analog_sample_count
        analog_val_rounded = round(analog_val_averaged / 5) * 5

        digital_val = self.digital_pin.value()
        self.power_pin.off()

        self._debug(
            'Moisture: read averaged:{}  final:{}'.format(analog_val_averaged, analog_val_rounded))

        if 10 < analog_val_rounded <= 1024:
            self._update_calibrated_values(
                min(self.analog_value_min, analog_val_rounded),
                max(self.analog_value_max, analog_val_rounded)
            )
        else:
            print('Moisture: invalid analog read, not updating values')

        return analog_val_rounded, digital_val

    def _normalize_analog_moisture(self, analog_val: int):
        """
        Normalize the analog value into a range 0-100 (percent).
        The normalization is done based on the bounds set up when calibrating.
        :param analog_val:
        :return:
        """
        analog_value_delta = self.analog_value_max - self.analog_value_min
        if analog_value_delta == 0:
            analog_value_delta = 1

        sensor_val_normalized = (analog_val - self.analog_value_min) / analog_value_delta

        moisture_level_percent = round((1 - sensor_val_normalized) * 100)

        if moisture_level_percent < 0:
            moisture_level_percent = 0
        elif moisture_level_percent > 100:
            moisture_level_percent = 100

        print('Moisture: normalizing moisture   {} -> {}    (calibration values {}/{})'.format(
            analog_val,
            moisture_level_percent,
            self.analog_value_min,
            self.analog_value_max)
        )

        return moisture_level_percent


class WaterPump:
    def __init__(self, control_pin: Pin, signal_led: Led = None):
        """
        :param control_pin: An OUT pin which is used to control the pump. Turning the pin on will
                            run the pump. Turning it off will stop pumping.
        """
        self.control_pin = control_pin
        self.signal_led = signal_led

    def do_pumping(self, duration_ms: int):
        """
        Run the pump for the given length of time.

        Blocking method. Will return once the pumping has stopped.

        :param duration_ms: the duration of the watering, in milliseconds
        """
        print('Going to pump water for {} milliseconds'.format(duration_ms))

        if self.signal_led:
            self.signal_led.on()

        self.control_pin.on()
        sleep_ms(duration_ms)
        self.control_pin.off()

        if self.signal_led:
            self.signal_led.off()


class AM2120(BaseNodeMcuClass):
    """
    Adapter for the AM2120 temperature and humidity sensor.

    The sensor uses the same protocol as the DHT22-class of sensors. During testing
    I found out that in most cases every second read fails. I don't know why and the
    easiest fix is to just retry reading from the sensor again. That's what this class
    does.
    """

    def __init__(
            self,
            pin: Pin,
            verbose: bool = False,
            cache_ttl_ms: int = 2500):
        """
        :param pin: the data pin where to read the values
        :param cache_ttl_ms: as per the documentation, the sensor should be called at most once every
                             two seconds. In-memory "cache" is be used to return the previously
                             saved results if there has not been enough time since the last sensor
                             read. This parameter configures the length of time between two
                             consecutive real readings.
        """
        super(AM2120, self).__init__(verbose)

        self._dht_reader = dht.DHT22(pin)
        self._verbose = verbose
        self._cache_ttl_ms = cache_ttl_ms

        self._last_read_ticks = None
        self._cached_temperature = None
        self._cached_humidity = None

    def read_values(
            self,
            read_attempts: int = 5,
            backoff_ms: int = 150,
            allow_cache: bool = True
    ):
        """
        :param read_attempts: number of read attempts to make before giving up and returning None
        :param backoff_ms: milliseconds to wait before retrying to read the sensor if the previous
                           read failed
        :param allow_cache: use the cache. See cache_ttl_ms parameter of this class for more details.
        :return: tuple (temp: float, humidity: float).
                 temp is in degrees Celsius, humidity a percentage.
                 Both values may also be None if the reading from the sensor fails multiple times
                 in a row.
        """
        read_ok = False

        if allow_cache and self._should_read_from_cache():
            self._debug('AM2120 cache hit, returning stored values')
            return self._cached_temperature, self._cached_humidity

        self._debug('AM2120 cache miss')

        for i in range(read_attempts):
            self._debug('AM2120 read attempt {}/{}'.format(i, read_attempts - 1))

            try:
                self._dht_reader.measure()
                read_ok = True
                self._debug('AM2120: Read ok: {} / {}'.format(
                    self._dht_reader.temperature(),
                    self._dht_reader.humidity())
                )
                break
            except Exception as e:
                self._debug('AM2120 attempt to read failed: {}'.format(e))
                sleep_ms(backoff_ms)

        if read_ok:
            self._cached_temperature = self._dht_reader.temperature()
            self._cached_humidity = self._dht_reader.humidity()
            self._last_read_ticks = ticks_ms()
            return self._dht_reader.temperature(), self._dht_reader.humidity()

        self._debug('AM2120: Read failed. Giving up.')

        return None, None

    def _should_read_from_cache(self):
        if self._last_read_ticks is None or \
                self._cached_temperature is None or \
                self._cached_humidity is None:
            return False

        ticks_now = ticks_ms()
        self._debug('AM2120 cache: now: {} vs last: {} (ttl: {})'.format(
            ticks_now, self._last_read_ticks, self._cache_ttl_ms)
        )

        return ticks_diff(ticks_now, self._last_read_ticks) < self._cache_ttl_ms
