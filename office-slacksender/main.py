from urandom import getrandbits
from machine import Pin
from neopixel import NeoPixel
from time import sleep, sleep_ms
import urequests


def randint(start, stop=None):
    """
    Get a random integer from the interval [start, stop) (start-inclusive, stop-exclusive).
    :param start:
    :param stop:
    :return: A single random integer.
    """
    if stop is None:
        stop = start
        start = 0
    upper = stop - start
    bits = 0
    pwr2 = 1
    while upper > pwr2:
        pwr2 <<= 1
        bits += 1
    while True:
        r = getrandbits(bits)
        if r < upper:
            break
    return r + start


#########

def wifi_connect(ssid, password):
    """
    Connect the device to a wifi network.
    :param ssid: the SSID of the wifi network to connect to
    :param password: the password of the wifi network
    :return:
    """
    import network
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect(ssid, password)
        while not wlan.isconnected():
            pass

    print('network config:', wlan.ifconfig())


class LedController:
    # NUM_LEDS = 15
    # pin = Pin(2, Pin.OUT)
    # np = NeoPixel(pin, NUM_LEDS)
    #
    # flash_pin = Pin(0, Pin.IN)
    #
    # default_color = (100, 0, 0)

    COLOR_OFF = (0, 0, 0)
    COLOR_RED = (255, 0, 0)
    COLOR_GREEN = (0, 255, 0)
    COLOR_BLUE = (0, 0, 255)

    def __init__(self, data_pin_number, led_count, default_color=COLOR_RED):
        self.led_count = led_count
        self.default_color = default_color
        self._neopixel = NeoPixel(Pin(data_pin_number, Pin.OUT), led_count)

    def on(self, led_id=None, color=None):
        """
        Turn a LED on

        :param led_id: the ID of the led, zero-indexed. If None, all LEDs are lit up
        :param color: a three-item RGB tuple, e.g. (255, 255, 100). If None, the default color of
                      the LedController is used
        """
        if not self._led_id_valid(led_id):
            return  # How to report errors?

        color = self._default_color_if_none(color)

        if led_id is None:
            for i in range(0, self.led_count):
                self._neopixel[i] = color
        else:
            self._neopixel[led_id] = color

        self._neopixel.write()

    def off(self, led_id=None):
        """
        Turn a LED off

        :param led_id: the ID of the led, zero-indexed. If None, all LEDs are turned off
        """
        if not self._led_id_valid(led_id):
            return  # How to report errors?

        if led_id is None:
            for i in range(0, self.led_count):
                self._neopixel[i] = self.COLOR_OFF
        else:
            self._neopixel[led_id] = self.COLOR_OFF

        self._neopixel.write()

    def flash(self, led_id=None, color=None, on_duration_ms=250):
        """
        Flash a LED on and off.

        :param led_id: the ID of the led, zero-indexed. If None, all LEDs will be flashed
        :param color: a three-item RGB tuple, e.g. (255, 255, 100). If None, the default color of
                      the LedController is used
        :param on_duration: the duration of the flash
        :return:
        """
        self.on(led_id, color)
        sleep_ms(on_duration_ms)
        self.off(led_id)

    def _led_id_valid(self, led_id):
        """Check if the given led ID is in a valid range - if such led exists"""
        return 0 <= led_id < self.led_count

    def _default_color_if_none(self, color):
        """
        Return the parameter color if it's not None. If None, return the default color
        configured for the class.
        """
        return color if color is not None else self.default_color


class Keypad:
    pins_rows = [
        Pin(16, Pin.OUT),  # keypad 8 (row 1)
        Pin(5, Pin.OUT),  # keypad 7 (row 2)
        Pin(4, Pin.OUT),  # keypad 6 (row 3)
        Pin(0, Pin.OUT),  # keypad 5 (row 4)
    ]

    pins_cols = [
        Pin(2, Pin.IN, Pin.PULL_UP),  # keypad 4 (col 1)
        Pin(14, Pin.IN, Pin.PULL_UP),  # keypad 3 (col 2)
        Pin(12, Pin.IN, Pin.PULL_UP),  # keypad 2 (col 3)
        Pin(13, Pin.IN, Pin.PULL_UP),  # keypad 1 (col 4)
    ]

    position_mappings = {
        # Char -> [col, row]
        '1': [0, 0],
        '2': [1, 0],
        '3': [2, 0],
        'A': [3, 0],
        '4': [0, 1],
        '5': [1, 1],
        '6': [2, 1],
        'B': [3, 1],
        '7': [0, 2],
        '8': [1, 2],
        '9': [2, 2],
        'C': [3, 2],
        '*': [0, 3],
        '0': [1, 3],
        '#': [2, 3],
        'D': [3, 3],
    }

    PIN_OUT_ON = 0
    PIN_OUT_OFF = 1

    def _check_position(self, col, row) -> bool:
        """
        Check if the button on position col,row is pressed. True for pressed, False for non-pressed
        """
        self.pins_rows[row].value(self.PIN_OUT_ON)
        pressed = self._is_pressed(self.pins_cols[col])
        self.pins_rows[row].value(self.PIN_OUT_OFF)

        return pressed

    @staticmethod
    def _is_pressed(pin):
        return pin.value() == 0

    def get_pressed_keys(self):
        """
        Get all pressed keys as an array of their alphanumeric symbols.
        :return: List of strings, e.g. ['1', 'A', '#']
        """
        result = []
        for char, position in self.position_mappings.items():
            if self._check_position(*position):
                result.append(char)

        return result


######  BELOW HERE IS NOT LIB

class HttpException(Exception):

    def __init__(self, *args: object) -> None:
        super().__init__(*args)


class Slack:
    def __init__(self, token):
        self.token = token

    def send_message(self, channel_name: str, message: str):
        """Send a Slack message to the given channel (e.g. #backend)"""
        json_payload = {
            'channel': channel_name,
            'text': message
        }

        response = urequests.post(
            'https://slack.com/api/chat.postMessage',
            headers={
                'Authorization': 'Bearer {}'.format(self.token)
            },
            json=json_payload
        )

        response_json = response.json()
        print(response_json)

        return response_json['ok']


WIFI_SSID = 'Rohea'
WIFI_PASSWORD = 'paceOnEarth'
SLACK_TOKEN = 'xoxb-3990455078-840666438743-6tvZi6BjwfIECKPbhjUQyFSD'

SLACK_CHANNEL_SUURLAHETYSTO = '#suurlahetysto'
# SLACK_CHANNEL_SUURLAHETYSTO = '#bots-testing'

BUTTON_MESSAGE_MAPPING = {
    '1': 'martin: morning! :sunny:',
    '2': 'martin: signing off :wave:',
    '3': None,
    'A': ':flag-cz: -> :bowl_with_spoon:',
    '4': None,
    '5': None,
    '6': None,
    'B': ':flag-cz:  back at the office',
    '7': None,
    '8': None,
    '9': None,
    'C': None,
    '*': None,
    '0': None,
    '#': None,
    'D': None,
}

if __name__ == '__main__':
    print('Started')

    wifi_connect(WIFI_SSID, WIFI_PASSWORD)
    slack = Slack(SLACK_TOKEN)
    keypad = Keypad()
    led_controller = LedController(15, 15)

    while True:
        pressed_keys = keypad.get_pressed_keys()
        if pressed_keys:
            message = BUTTON_MESSAGE_MAPPING[pressed_keys[0]]
            if message is None:
                continue

            send_success = slack.send_message(SLACK_CHANNEL_SUURLAHETYSTO, message)

            if send_success:
                led_controller.flash(2, (0, 255, 0))
            else:
                led_controller.flash(2)

            sleep(1)

        sleep_ms(10)
