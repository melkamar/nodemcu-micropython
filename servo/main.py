from machine import Pin, PWM
import time

from umqtt.simple import MQTTClient

from machine import Pin, PWM
from neopixel import NeoPixel
from time import sleep

from urandom import *
import sys
import urequests

PAYLOAD_STATE_ON = b'ON'
PAYLOAD_STATE_OFF = b'OFF'

PAYLOAD_AVAILABLE = b'online'
PAYLOAD_NOT_AVAILABLE = b'offline'

#####################################################
#####################################################

TOPIC_STATUS_ACK = b'switch/curtains'
TOPIC_AVAILABILITY = b'switch/curtains/available'
TOPIC_STATUS_COMMAND = b'switch/curtains/set'

ROTATE_CW = 10
ROTATE_CCW = 100
DURATION_STD = 15
DURATION_SHORT = 0.3

LOCAL_STATE_UNDEFINED = 'undef'
LOCAL_STATE_OPEN = 'opened'
LOCAL_STATE_CLOSED = 'closed'

CURRENT_STATE = LOCAL_STATE_UNDEFINED

#####################################################
#####################################################

# pin = Pin(2, Pin.OUT)
servo = PWM(Pin(2), freq=50)


def connect_to_wifi():
    ESSID = 'Union cunts'
    PASSWORD = '1superSecret'

    import network
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect(ESSID, PASSWORD)
        while not wlan.isconnected():
            pass
    print('network config:', wlan.ifconfig())


def randrange(start, stop=None):
    if stop is None:
        stop = start
        start = 0
    upper = stop - start
    bits = 0
    pwr2 = 1
    while upper > pwr2:
        pwr2 <<= 1
        bits += 1
    while True:
        r = getrandbits(bits)
        if r < upper:
            break
    return r + start


def randint(start, stop):
    return randrange(start, stop + 1)


def signal_error():
    rotate(ROTATE_CW, DURATION_SHORT)
    rotate(ROTATE_CCW, DURATION_SHORT)
    rotate(ROTATE_CW, DURATION_SHORT)
    rotate(ROTATE_CCW, DURATION_SHORT)


def rotate(direction, duration):
    servo.duty(direction)
    sleep(duration)
    servo.deinit()


# Received messages from subscriptions will be delivered to this callback
def sub_callback(topic, msg):
    global CURRENT_STATE

    if topic == TOPIC_STATUS_COMMAND:
        # Click on a switch in HASS - change the state of the device and report to mqtt
        if msg == PAYLOAD_STATE_ON:
            mqtt_client.publish(TOPIC_STATUS_ACK, PAYLOAD_STATE_ON)
            if CURRENT_STATE != LOCAL_STATE_OPEN:
                rotate(ROTATE_CCW, DURATION_STD)
                CURRENT_STATE = LOCAL_STATE_OPEN
        elif msg == PAYLOAD_STATE_OFF:
            mqtt_client.publish(TOPIC_STATUS_ACK, PAYLOAD_STATE_OFF)
            if CURRENT_STATE != LOCAL_STATE_CLOSED:
                rotate(ROTATE_CW, DURATION_STD)
                CURRENT_STATE = LOCAL_STATE_CLOSED

        else:
            signal_error()
    else:
        signal_error()


def do_sub():
    server = '192.168.0.52'
    c = MQTTClient("umqtt_client", server, user='homeassistant', password='mqttpwd')
    c.set_callback(sub_callback)
    c.connect()
    c.subscribe(TOPIC_STATUS_COMMAND)
    return c


connect_to_wifi()

mqtt_client = do_sub()
mqtt_client.publish(TOPIC_AVAILABILITY, PAYLOAD_AVAILABLE)

while True:
    mqtt_client.wait_msg()
