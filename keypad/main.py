"""
The way this works
- to detect if a button at position x,y is pressed, we need to find the two pins corresponding to
  the column x and row y
- the column is always a read-only pin, the row pin is always a write-only pin
- turn all write pins (all rows) off. Turn only the y-row on
- read the value from the x-pin. If 1, then the electricity flows through the button at
  position x,y -> it is pressed!

Well, this is actually the opposite, because the electricity flows through the button _unless_
  it is pressed (pressing the button grounds the circuit), but you get the idea.
"""

# from machine import Pin
# from time import sleep
import micropython

from neopixel import NeoPixel
from machine import Pin
from machine import Timer
from time import sleep_ms

led_pin = Pin(15, Pin.OUT)
led = NeoPixel(led_pin, 15)

pin_out = Pin(5, Pin.OUT)
pin_in = Pin(4, Pin.IN, Pin.PULL_UP)

pin_in_flash = Pin(0, Pin.IN, Pin.PULL_UP)

PIN_OUT_ON = 0
PIN_OUT_OFF = 1


def on(i, color=(255, 0, 0)):
    led[i] = color
    led.write()


def off(i):
    led[i] = (0, 0, 0)
    led.write()


def flash(id, color=(255, 0, 0)):
    on(id, color)
    sleep_ms(100)
    off(id)


pins_rows = [
    Pin(16, Pin.OUT),  # keypad 8 (row 1)
    Pin(5, Pin.OUT),  # keypad 7 (row 2)
    Pin(4, Pin.OUT),  # keypad 6 (row 3)
    Pin(0, Pin.OUT),  # keypad 5 (row 4)
]

pins_cols = [
    Pin(2, Pin.IN, Pin.PULL_UP),  # keypad 4 (col 1)
    Pin(14, Pin.IN, Pin.PULL_UP),  # keypad 3 (col 2)
    Pin(12, Pin.IN, Pin.PULL_UP),  # keypad 2 (col 3)
    Pin(13, Pin.IN, Pin.PULL_UP),  # keypad 1 (col 4)
]


def check_position(col, row):
    pins_rows[row].value(PIN_OUT_ON)
    pressed = is_pressed(pins_cols[col])
    pins_rows[row].value(PIN_OUT_OFF)

    return pressed


def is_pressed(pin):
    return pin.value() == 0


position_mappings = {
    # Char -> [col, row]
    '1': [0, 0],
    '2': [1, 0],
    '3': [2, 0],
    'A': [3, 0],
    '4': [0, 1],
    '5': [1, 1],
    '6': [2, 1],
    'B': [3, 1],
    '7': [0, 2],
    '8': [1, 2],
    '9': [2, 2],
    'C': [3, 2],
    '*': [0, 3],
    '0': [1, 3],
    '#': [2, 3],
    'D': [3, 3],
}

walk_order = '1234567890ABCD*#'[:15]  # Only want the first 15 chars because of 15 leds

# TODO: clean  this up and upload the code to git

time_elapsed = 0
while True:
    for i, char in enumerate(walk_order):
        # TODO: build a result matrix of what is and isn't pressed and provide that as the API of a class -> wrap the keyboard reading in that class
        if check_position(*(position_mappings[char])):
            on(i)
        else:
            off(i)

        # TODO: write somewhere about how the keypad is incorrectly shown!
