# NodeMCU and Micropython

A repository of my more or less successful Micropython-on-NodeMCU tinkering

## This repository is in very much of a TODO state
- [ ] Write READMEs for all the project directories (diagrams etc.)
- [ ] Write an index here to link to the projects with short descriptions
- [ ] Archive the individual Gitlab repositories (this repo is a "monorepo" for the micropython projects)

## Commands

### Flash Micropython onto a NodeMCU
```
$ MICROPYTHON_IMAGE='/Users/melka/Downloads/esp8266-20190529-v1.11.bin'

$ python -m esptool --port /dev/tty.usbserial-14* --baud 115200 erase_flash
$ python -m esptool --port /dev/tty.usbserial-14* --baud 115200 write_flash 0 "$MICROPYTHON_IMAGE" 
```

## Links
This is the collection of links I have been using for my attempts. Some will be English, but some
will be Czech. Sorry for that, if you are a non-Czech reading this.

### Tutorials, datasheets
- [NodeMCU pin diagram](_images/nodemcu-pins.jpg) for both v2 and v3
- [Micropython at Czech Technical University](https://naucse.python.cz/lessons/intro/micropython/) thanks to Miro Hrončok and Petr Viktorin for a great MI-PYT course at CTU! 
- [Micropython on ESP8266](http://docs.micropython.org/en/v1.9.3/esp8266/esp8266/tutorial/index.html) official documentation
- [Keypad datasheet](https://arduino-shop.cz/docs/produkty/0/775/eses1500635994.pdf) (cz)
    - TODO write warning about wrong pin ordering
- [General guide to the ESP8266 hardware](https://tttapa.github.io/ESP8266/Chap01%20-%20ESP8266.html) aimed at IOT beginners

### Purchase links (no affiliates)
- [Keypad](https://arduino-shop.cz/arduino/824-arduino-klavesnice-membranova-4x4.html)
