from machine import Pin, ADC
from utime import sleep_ms, ticks_ms, ticks_diff

from lib import MoistureSensor, Led, WaterPump, Mqtt, NodemcuPin


# noinspection PyBroadException
class MqttWaterer:
    MODE_READING_MOISTURE = 0  # Normal operation
    MODE_ERROR = 1  # Error state. Will not perform anything unless unlocked via "recover_error()"

    # MOISTURE_READ_INTERVAL = 5000  # 60000  # Interval in milliseconds between readings of moisture

    TOPIC_MOISTURE = 'moisture'
    TOPIC_COMMAND_WATERING = 'cmd/watering'

    def __init__(
            self,
            waterer_mqtt_topic: str,
            moisture_sensor: MoistureSensor,
            control_pin: Pin,
            signal_led: Led,
            pump: WaterPump,
            mqtt_client: Mqtt,
            watering_duration_ms: int = 5000,
            start_watering_threshold: int = 10,
            moisture_read_interval: int = 60000
    ):
        """
        :param waterer_mqtt_topic: The MQTT topic used to communicate to and from this device. This
                                   parameter is used as a namespace. For example, given a value of
                                   "home/bedroom/window_plant", the corresponding MQTT topics used
                                   by the device will be:
                                     - "home/bedroom/window_plant/moisture",
                                     - "home/bedroom/window_plant/water_command",
                                     - "home/bedroom/window_plant/available" etc.
                                   The full list of the concrete topics should be in the readme.
        :param moisture_sensor: The sensor to use for reading moisture values.
        :param control_pin: An input pin used to receive commands from the user.
        :param signal_led: An LED used to signal status to the user.
        :param mqtt_client: An MQTT client used to send and receive data to the control hub.
        :param watering_duration_ms: Duration of watering when low moisture threshold is hit, in
                                     milliseconds
        :param start_watering_threshold: The moisture percentage threshold when to start watering.
                                         10 means the watering should start once the moisture
                                         percentage drops below 10%.
        """
        self.waterer_mqtt_topic = waterer_mqtt_topic.strip('/')
        self.moisture_sensor = moisture_sensor
        self.control_pin = control_pin
        self.signal_led = signal_led
        self.pump = pump
        self.mqtt_client = mqtt_client
        self.state = self.MODE_READING_MOISTURE
        self.watering_duration_ms = watering_duration_ms
        self.start_watering_threshold = start_watering_threshold
        self.moisture_read_interval = moisture_read_interval

        self.last_moisture_read_ts = None
        """
        Timestamp of the last sensor reading
        """

        self.consecutive_watering_commands = 0
        """
        Number of watering commands received consecutively
        That is, not a single execution of the loop where the watering command was off.
        """

        self.flag_do_watering = False
        """
        If true, watering will be executed during the next loop.
        """

        if self.mqtt_client:
            self.mqtt_client.subscribe(self._get_full_topic(self.TOPIC_COMMAND_WATERING),
                                       self._mqtt_callback)

    def _mqtt_callback(self, topic: str, message: str):
        if topic == self._get_full_topic(self.TOPIC_COMMAND_WATERING):
            self.flag_do_watering = True
            self.watering_duration_ms = int(message)

    def run(self):
        """
        Blocking method, this is the control loop of the whole watering process.
        """
        print('MqttWaterer: Running the main loop')

        while True:
            if self.state == self.MODE_READING_MOISTURE:
                self._loop_body_state_normal()
            elif self.state == self.MODE_ERROR:
                self._loop_body_state_error()

    def _loop_body_state_normal(self):
        try:
            moisture_percent = self._moisture_read_routine()
            if moisture_percent is not None:
                self._watering_routine(moisture_percent)

            self._sleep_ms_with_button_handler(self.moisture_read_interval // 100)
        except Exception:
            # If any kind of exception happens, give control back to the loop control
            return

    def _loop_body_state_error(self):
        print('MqttWaterer: In error mode')
        self.signal_led.flash()
        self.signal_led.flash()
        self._sleep_ms_with_button_handler(200)

    def _moisture_read_routine(self):
        """
        Read and send the moisture level, if enough time has passed from the last read.
        """
        if self.last_moisture_read_ts is not None and self._count_ms_passed_since(
                self.last_moisture_read_ts) < self.moisture_read_interval:
            print('MqttWaterer: skipping reading (current/last: {}/{})'.format(ticks_ms(),
                                                                               self.last_moisture_read_ts))
            return None

        self.signal_led.flash()

        print('MqttWaterer: doing the reading (current/last tick: {}/{})'.format(ticks_ms(),
                                                                                 self.last_moisture_read_ts))
        self.last_moisture_read_ts = ticks_ms()

        moisture_percent = self.moisture_sensor.read_moisture_percent()

        if self.mqtt_client:
            self.mqtt_client.send_message(
                self._get_full_topic(self.TOPIC_MOISTURE), str(moisture_percent)
            )

        return moisture_percent

    def _watering_routine(self, moisture_percent):
        """
        Check if a watering command is waiting to be processed. If so, do the watering.

        If there were too many watering commands in a row, the sensor will assume an error occurred
        and will make itself go into the error mode, just to be safe. The error mode can be manually
        fixed just by pressing the button.
        """
        if self.mqtt_client:
            self.mqtt_client.check_msg()

        if moisture_percent is not None and moisture_percent < self.start_watering_threshold:
            self.flag_do_watering = True

        if self.flag_do_watering:
            print('MqttWaterer: Turning on the pump. Consecutive: {}'.format(
                self.consecutive_watering_commands)
            )
            self.flag_do_watering = False

            if self.consecutive_watering_commands > 3:
                self._enable_error_mode()
                return

            self.pump.do_pumping(self.watering_duration_ms)
            self.consecutive_watering_commands += 1
        else:
            self.consecutive_watering_commands = 0

    def _enable_error_mode(self):
        print('MqttWaterer: Going to error mode')
        self.state = self.MODE_ERROR

    def _recover_error(self):
        """
        Remove the error state from the waterer.

        This method only removes the error flag, it is up to the caller to ensure that the error
        condition that led here is removed.
        """
        print('MqttWaterer: Recovering from error mode')
        self.state = self.MODE_READING_MOISTURE
        self.consecutive_watering_commands = 0

    def _get_full_topic(self, topic: str) -> str:
        """
        Get the full topic path, including the "namespace" of the device"
        :param topic: the concrete topic to use, e.g. "moisture", "available", etc.
        :return: the full topic, e.g. "home/bedroom/plant1/moisture"
        """
        return "{}/{}".format(self.waterer_mqtt_topic, topic)

    def _button_pressed_routine(self):
        if self.control_pin.value() == NodemcuPin.BUTTON_NOT_PRESSED:
            return

        print('MqttWaterer: button press detected')

        if self.state == self.MODE_ERROR:
            self._recover_error()
            return

        if self.state == self.MODE_READING_MOISTURE:
            if self._is_button_long_pressed(1000):
                self.moisture_sensor.reset_calibration()
                self.signal_led.flash(2000)

    def _is_button_long_pressed(self, long_press_duration_ms: int):
        """
        Check if the button is long-pressed. This method is blocking.

        :param long_press_duration_ms: the duration during which the button must be pressed
        :return: True if the button is long-pressed, false otherwise
        """
        init_ticks = ticks_ms()
        while self._count_ms_passed_since(init_ticks) < long_press_duration_ms:
            if self.control_pin.value() == NodemcuPin.BUTTON_NOT_PRESSED:
                return False
            sleep_ms(10)

        return True

    def _sleep_ms_with_button_handler(self, milliseconds_to_sleep: int):
        init_ticks = ticks_ms()
        while self._count_ms_passed_since(init_ticks) < milliseconds_to_sleep:
            self._button_pressed_routine()
            sleep_ms(10)

    @staticmethod
    def _count_ms_passed_since(initial_ticks_ms: int) -> int:
        """Count the number of milliseconds between now and some initial tick number."""
        return ticks_diff(ticks_ms(), initial_ticks_ms)


def init_waterer() -> MqttWaterer:
    print('Initializing the waterer')

    ###
    # Configuration
    ###
    wifi_ssid = 'Union cunts'
    wifi_password = '1superSecret'
    mqtt_server = '192.168.0.52'
    mqtt_user = 'homeassistant'
    mqtt_password = 'mqttpwd'
    mqtt_device_topic = 'home/bedroom/plant1'

    analog_pin = NodemcuPin.get_analog_pin_read()

    moisture_sensor_power = NodemcuPin.get_pin_write(5)
    moisture_sensor_digital_read = NodemcuPin.get_pin_read(6)

    onboard_led = Led.get_onboard_led()

    moisture_sensor = MoistureSensor(moisture_sensor_power, analog_pin,
                                     moisture_sensor_digital_read)
    button_pin = Pin(0, Pin.IN)

    # wifi = Wifi(wifi_ssid, wifi_password)
    # wifi.connect()
    #
    # mqtt = Mqtt(mqtt_server, mqtt_user, mqtt_password, wifi)

    pump = WaterPump(NodemcuPin.get_pin_write(1), onboard_led)

    return MqttWaterer(
        mqtt_device_topic,
        moisture_sensor,
        button_pin,
        onboard_led,
        pump,
        None,
        watering_duration_ms=6000,
        start_watering_threshold=10,
        moisture_read_interval=10000
    )


def main():
    waterer_object = init_waterer()
    waterer_object.run()


if __name__ == '__main__':
    main()
