# import paho.mqtt.client as mqtt
#
#
# # The callback for when the client receives a CONNACK response from the server.
# def on_connect(client, userdata, flags, rc):
#     print("Connected with result code " + str(rc))
#
#     # Subscribing in on_connect() means that if we lose the connection and
#     # reconnect then subscriptions will be renewed.
#     client.subscribe("test/strip/switch1")
#
#
# # The callback for when a PUBLISH message is received from the server.
# def on_message(client, userdata, msg):
#     print(msg.topic + " " + str(msg.payload))
#
#
# if __name__ == '__main__':
#     client = mqtt.Client()
#     client.on_connect = on_connect
#     client.on_message = on_message
#
#     client.connect("hassio.local", 1883, 60)
#     print('Connected')
#     # Blocking call that processes network traffic, dispatches callbacks and
#     # handles reconnecting.
#     # Other loop*() functions are available that give a threaded interface and a
#     # manual interface.
#     client.loop_forever()

import paho.mqtt.subscribe
import paho.mqtt.publish
import datetime

PAYLOAD_STATE_ON = 'ON'
PAYLOAD_STATE_OFF = 'OFF'

PAYLOAD_AVAILABLE = 'online'
PAYLOAD_NOT_AVAILABLE = 'offline'

STATUS_ACK = 'test/strip/light/status'
STATUS_COMMAND = 'test/strip/light/switch'

BRIGHTNESS_ACK = 'test/strip/brightness/status'
BRIGHTNESS_COMMAND = 'test/strip/brightness/set'

RGB_ACK = 'test/strip/rgb/status'
RGB_COMMAND = 'test/strip/rgb/set'

# TOPIC_AVAILABILITY = 'test/strip/switch1/available'

SUBSCRIBE_TOPICS = [
    STATUS_ACK, STATUS_COMMAND,
    BRIGHTNESS_ACK, BRIGHTNESS_COMMAND,
    RGB_ACK, RGB_COMMAND
]


def log(msg: str):
    print(f'{datetime.datetime.now().timestamp()} | {msg}')


def publish(topic: str, payload: str):
    log(f'Publish "{payload}" onto "{topic}"')
    paho.mqtt.publish.single(topic,
                             payload=payload,
                             hostname="hassio.local",
                             auth={'username': 'homeassistant', 'password': 'mqttpwd'})


def subscribe(callback):
    """callback: on_message(client, userdata, message)"""
    paho.mqtt.subscribe.callback(
        callback,
        SUBSCRIBE_TOPICS,
        hostname="hassio.local",
        auth={'username': 'homeassistant', 'password': 'mqttpwd'})


#
#
# AIzaSyD3Ipdve1jOEzaO5yiRsWa_ZfJp7AlrljM - google cloud api key


#
#
#

def sub_callback(client, userdata, message):
    log(f'{message.topic}: {message.payload}')
    if message.topic == STATUS_COMMAND:
        # Click on a switch in HASS - change the state of the device and report to mqtt
        publish(STATUS_ACK, message.payload)
    elif message.topic == RGB_COMMAND:
        publish(RGB_ACK, message.payload)


def set_state(new_state):
    topic = 'test/strip/switch1'

    publish(topic, new_state)


if __name__ == '__main__':
    # publish(TOPIC_AVAILABILITY, PAYLOAD_AVAILABLE)
    subscribe(sub_callback)

