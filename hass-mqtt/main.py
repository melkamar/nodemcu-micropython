from umqtt.simple import MQTTClient

from machine import Pin
from neopixel import NeoPixel
from time import sleep

from urandom import *
import sys
import urequests

PAYLOAD_STATE_ON = b'ON'
PAYLOAD_STATE_OFF = b'OFF'

PAYLOAD_AVAILABLE = b'online'
PAYLOAD_NOT_AVAILABLE = b'offline'

STATUS_ACK = b'test/strip/light/status'
STATUS_COMMAND = b'test/strip/light/switch'

BRIGHTNESS_ACK = b'test/strip/brightness/status'
BRIGHTNESS_COMMAND = b'test/strip/brightness/set'

RGB_ACK = b'test/strip/rgb/status'
RGB_COMMAND = b'test/strip/rgb/set'

TOPIC_SUB = b'test/strip/+/+'

NUM_LEDS = 15
pin = Pin(2, Pin.OUT)
np = NeoPixel(pin, NUM_LEDS)

CURRENT_STATE_ON = True
CURRENT_COLOR_R = 0
CURRENT_COLOR_G = 0
CURRENT_COLOR_B = 0
CURRENT_BRIGHTNESS = 0


def connect_to_wifi():
    ESSID = 'Union cunts'
    PASSWORD = '1superSecret'

    import network
    wlan = network.WLAN(network.STA_IF)
    wlan.active(True)
    if not wlan.isconnected():
        print('connecting to network...')
        wlan.connect(ESSID, PASSWORD)
        while not wlan.isconnected():
            pass
    print('network config:', wlan.ifconfig())


def randrange(start, stop=None):
    if stop is None:
        stop = start
        start = 0
    upper = stop - start
    bits = 0
    pwr2 = 1
    while upper > pwr2:
        pwr2 <<= 1
        bits += 1
    while True:
        r = getrandbits(bits)
        if r < upper:
            break
    return r + start


def randint(start, stop):
    return randrange(start, stop + 1)


def light_random_colors():
    for led_id in range(0, NUM_LEDS):
        np[led_id] = (
            randint(0, 255),
            randint(0, 255),
            randint(0, 255),
        )
    np.write()


def light_update_from_globals():
    brightness_multiplier = CURRENT_BRIGHTNESS / 255

    if CURRENT_STATE_ON:
        for led_id in range(0, NUM_LEDS):
            np[led_id] = (
                int(CURRENT_COLOR_R * brightness_multiplier),
                int(CURRENT_COLOR_G * brightness_multiplier),
                int(CURRENT_COLOR_B * brightness_multiplier),
            )
        np.write()
    else:
        light_turn_off()


def light_turn_off():
    for led_id in range(0, NUM_LEDS):
        np[led_id] = (0, 0, 0)
    np.write()


def signal_error(r, g, b):
    print('{} {} {}'.format(r, g, b, ))
    # from sys import exit
    # exit(0)
    r, g, b = int(r), int(g), int(b)
    for i in range(0, 3):
        for led_id in range(0, NUM_LEDS):
            np[led_id] = (r, g, b)
        np.write()
        sleep(0.25)

        light_turn_off()
        sleep(0.25)


# Received messages from subscriptions will be delivered to this callback
def sub_callback(topic, msg):
    global CURRENT_STATE_ON
    global CURRENT_COLOR_R
    global CURRENT_COLOR_G
    global CURRENT_COLOR_B
    global CURRENT_BRIGHTNESS

    if topic == STATUS_COMMAND:
        # Click on a switch in HASS - change the state of the device and report to mqtt
        if msg == PAYLOAD_STATE_ON:
            # light_random_colors()
            CURRENT_STATE_ON = True
            mqtt_client.publish(STATUS_ACK, PAYLOAD_STATE_ON)
        elif msg == PAYLOAD_STATE_OFF:
            CURRENT_STATE_ON = False
            mqtt_client.publish(STATUS_ACK, PAYLOAD_STATE_OFF)
        else:
            signal_error(255, 0, 0)

    elif topic == RGB_COMMAND:
        r, g, b = msg.decode().split(',')
        CURRENT_COLOR_R = int(r)
        CURRENT_COLOR_G = int(g)
        CURRENT_COLOR_B = int(b)

        mqtt_client.publish(
            RGB_ACK,
            '{},{},{}'.format(
                CURRENT_COLOR_R,
                CURRENT_COLOR_G,
                CURRENT_COLOR_B).encode()
        )

    elif topic == BRIGHTNESS_COMMAND:
        CURRENT_BRIGHTNESS = int(msg.decode())
        mqtt_client.publish(BRIGHTNESS_ACK, msg)

    light_update_from_globals()


def do_sub():
    server = '192.168.0.52'
    c = MQTTClient("umqtt_client", server, user='homeassistant', password='mqttpwd')
    c.set_callback(sub_callback)
    c.connect()
    c.subscribe(TOPIC_SUB)
    return c


connect_to_wifi()
light_random_colors()

mqtt_client = do_sub()
# mqtt_client.publish(TOPIC_AVAILABILITY, PAYLOAD_AVAILABLE)
mqtt_client.publish(STATUS_ACK, PAYLOAD_STATE_ON)

while True:
    mqtt_client.wait_msg()
