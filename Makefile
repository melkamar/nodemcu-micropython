MICROPYTHON_IMAGE = micropython_images/esp8266-20191220-v1.12.bin

PHONY: flash
flash:
	@echo "Flashing micropython image: $(MICROPYTHON_IMAGE)"
	python -m esptool --port /dev/tty.usbserial-14* --baud 115200 erase_flash
	python -m esptool --port /dev/tty.usbserial-14* --baud 115200 write_flash 0 $(MICROPYTHON_IMAGE)

PHONY: clear
clear:
